default:
	$(MAKE) -C ./xmmintrin
	$(MAKE) -C ./single
	$(MAKE) -C ./openmp
	$(MAKE) -C ./openmpi
	$(MAKE) -C ./thread
	$(MAKE) -C ./atomic

.PHONY:
	clean

clean:
	rm -f xmmintrin/*.o xmmintrin/mul
	rm -f single/*.o single/mul
	rm -f openmpi/*.o openmpi/mul
	rm -f openmp/*.o openmp/mul
	rm -f thread/*.o thread/mul
	rm -f atomic/*.o atomic/mul
