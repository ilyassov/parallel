#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <fstream>
#include <algorithm>

#ifdef OPEN_MP
#include <omp.h>
#endif

#if defined(THREADS) || defined(ATOMIC)
#include <thread>
#define NTHREADS 8
#endif

#ifdef ATOMIC
#include <atomic>
#endif

#ifdef XMMINTRIN
#include <immintrin.h>
#endif

#ifdef OPEN_MPI
#include <mpi.h>
#endif

class Matrix{
public:
    #ifdef OPEN_MPI
    double *_data;
    #endif
    double **data;
    size_t size;
    int world_size;

    Matrix(size_t n);
    Matrix(std::string filename);

    bool operator==(const Matrix &matr);
    Matrix operator=(const Matrix &matr);
    Matrix operator*(Matrix &matr);

    double get(size_t i, size_t j);
    void set(size_t i, size_t j, double value);
    void add(size_t i, size_t j, double value);
    size_t getSize();
    double random(void);
    void randomFill();
    void zero();
    void print();

    #if defined(THREADS) || defined(ATOMIC)
    friend void product(Matrix &matr, Matrix &matr1, Matrix &matr2, size_t thread_numb);
    #endif

    ~Matrix();
};