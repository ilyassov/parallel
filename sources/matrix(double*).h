#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <omp.h>
#include <fstream>
#include <algorithm>

class Matrix{
//    double **data;
    double * data;
    size_t size;
public:
    Matrix(size_t n);
    Matrix(std::string filename);

    bool operator==(const Matrix &matr);
    Matrix operator=(const Matrix &matr);
    Matrix operator*(const Matrix &matr);

    size_t getSize();
    double random(void);
    void randomFill();
    void zero();
    void print();

    ~Matrix();
};