#include "matrix.h"

/*
// Generates n*n sized matrix
Matrix::Matrix(size_t n) : size(n) {
    data = new double *[n];
    data[0] = new double[n * n];

    for (size_t i = 1; i < n; i++) {
        data[i] = data[i - 1] + n;
    }
}
*/

Matrix::Matrix(size_t n) : size(n) {
    data = new double[n * n];
}

Matrix::Matrix(std::string filename) {
    size_t i = 0, j = 0;
    std::string line;
    std::string delimeters = " {}";

    std::ifstream in(filename);

    std::getline(in, line, '\n');
    size = std::stoi(line);

/*    data = new double *[size];
    data[0] = new double[size * size];

    for (size_t i = 1; i < size; i++) {
        data[i] = data[i - 1] + size;
    }
*/
    data = new double [size * size];

    if (in.is_open()) {
        while (getline(in, line, ',')) {
            for (char ch : delimeters) {
                line.erase(std::remove(line.begin(), line.end(), ch), line.end());
            }

            if (i >= size) {
                std::cerr << "Too much data" << std::endl;
                exit(1);
            }

//            data[i][j++] = stof(line);

            data[i * size + j] = stof(line);

            if (++j == size) {
                ++i;
                j = 0;
            }
        }
    } else {
        std::cerr << "Error ocured while opening file!" << std::endl;
        exit(1);
    }

    in.close();
}

/*
// Checks if one matrix equals another
bool Matrix::operator==(const Matrix &matr) {
    double eps = 1e-5; 
    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            if (abs(data[i][j] - matr.data[i][j]) > eps)
                return false;
        }
    }
    return true;
}
*/

bool Matrix::operator==(const Matrix &matr) {
    double eps = 1e-5;
    size_t _size = size*size;
    for (size_t i = 0; i < _size; ++i) {
        if (abs(data[i] - matr.data[i]) > eps) {
            return false;
        }
    }
    return true;
}

/*
// Equates one matrix with another matrix
Matrix Matrix::operator=(const Matrix & matr) {
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            data[i][j] = matr.data[i][j];
        }
    }
    return (*this);
}
*/

Matrix Matrix::operator=(const Matrix & matr) {
    size_t _size = size*size;
    for (size_t i = 0; i < _size; i++) {
            data[i] = matr.data[i];
    }
    return (*this);
}

/*
// Returns multiplication of two n sized matrices
Matrix Matrix::operator*(const Matrix &matr) {
    Matrix answer(size);
    answer.zero();

    double tmp;
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            tmp = matr.data[i][j];
            matr.data[i][j] = matr.data[j][i];
            matr.data[j][i] = tmp;
        }
    }

    #ifdef OPENMP
    #pragma omp parallel
    {
    #pragma omp for
    #endif

    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            for (size_t k = 0; k < size; ++k) {
                answer.data[i][j] += data[i][k] * matr.data[k][j];
            }
        }
    }

    #ifdef OPENMP
    }
    #endif

    return answer;
}
*/

Matrix Matrix::operator*(const Matrix &matr) {
    Matrix answer(size);
    answer.zero();

    #ifdef OPENMP
    #pragma omp parallel
    {
    #pragma omp for
    #endif

    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            for (size_t k = 0; k < size; ++k) {
                answer.data[i*size + j] += data[i*size + k] * matr.data[k*size + j];
            }
        }
    }

    #ifdef OPENMP
    }
    #endif

    return answer;
}

// Returns Matrix size
size_t Matrix::getSize() {
    return size;
}

// Returns random double between 0 and 1
double Matrix::random(void) {
    return (double)(rand())/RAND_MAX;
}

// Fills matrix with random numbers between 0 and 1
/*
void Matrix::randomFill() {
    srand((unsigned int)time(0));

    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            data[i][j] = random();
        }
        
    }
}
*/

void Matrix::randomFill() {
    srand((unsigned int)time(0));
    size_t _size = size*size;
    for (size_t i = 0; i < _size; i++) {
        data[i] = random();
    }
}

/*
// Zero matrix
void Matrix::zero() {
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            data[i][j] = 0;
        }
    }
}
*/

void Matrix::zero() {
    size_t _size = size*size;
    for (size_t i = 0; i < _size; i++) {
            data[i] = 0;
    }
}

// Prints matrix
void Matrix::print() {
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            std::cout << data[i*size + j] << " ";
        }
        std::cout << std::endl;
    }
}

/*
Matrix::~Matrix() {
    delete[] data[0];
    delete[] data;
}
*/

Matrix::~Matrix() {
    delete[] data;
}