#include "matrix.h"

#ifdef ATOMIC
std::atomic<size_t> row(0);
#endif

// Generates n*n sized matrix
Matrix::Matrix(size_t n) : size(n) {
    #ifdef OPEN_MPI
    _data = new double[n * n];
    #endif
#ifdef XMMINTRIN
    data = new(std::align_val_t{ 32 }) double*[n];
    data[0] = new(std::align_val_t{ 32 }) double[n * n];
#else
    data =  new double *[n];
    data[0] = new double[n * n];
#endif

    for (size_t i = 1; i < n; i++) {
        data[i] = data[i - 1] + n;
    }
}

Matrix::Matrix(std::string filename) {
    size_t i = 0, j = 0;
    std::string line;
    std::string delimeters = " {}";

    std::ifstream in(filename);

    std::getline(in, line, '\n');
    size = std::stoi(line);

    data = new double *[size];
    data[0] = new double[size * size];

    #ifdef OPEN_MPI
    _data = new double[size *size];
    #endif

    for (size_t i = 1; i < size; i++) {
        data[i] = data[i - 1] + size;
    }

    if (in.is_open()) {
        while (getline(in, line, ',')) {
            for (char ch : delimeters) {
                line.erase(std::remove(line.begin(), line.end(), ch), line.end());
            }

            if (i >= size) {
                std::cerr << "Too much data" << std::endl;
                exit(1);
            }

            #ifdef OPEN_MPI
            _data[i * size + j] = stof(line);
            if (++j == size) {
                ++i;
                j = 0;
            }
            #else
            data[i][j++] = stof(line);

            if (j == size) {
                ++i;
                j = 0;
            }
            #endif
        }
    } else {
        std::cerr << "Error ocured while opening file!" << std::endl;
        exit(1);
    }
    in.close();
}

// Checks if one matrix equals another
bool Matrix::operator==(const Matrix &matr) {
    if ((*this).size != matr.size) {
        std::cerr << "Wrong sizes!" << std::endl;
    }
    double eps = 1e-3;
    #ifdef OPEN_MPI
    size_t _size = size * size;
    for (size_t i = 0; i < _size; ++i) {
        if (abs(_data[i] - matr._data[i]) > eps) {
            return false;
        }
    }
    #else
    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            if (abs(data[i][j] - matr.data[i][j]) > eps)
                return false;
        }
    }
    #endif
    return true;
}

// Equates one matrix with another matrix
Matrix Matrix::operator=(const Matrix & matr) {
    #ifdef OPEN_MPI
    size_t _size = size*size;
    for (size_t i = 0; i < _size; i++) {
            _data[i] = matr._data[i];
    }
    #else
    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            data[i][j] = matr.data[i][j];
        }
    }
    #endif
    return (*this);
}

// Returns multiplication of two n sized matrices
Matrix Matrix::operator*(Matrix &matr) {

    Matrix answer(size);
    answer.zero();

    #ifdef XMMINTRIN
    Matrix matr2(size);
    matr2.zero();
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            matr2.set(i, j, matr.get(j, i));
        }
    }
    #endif

    #if defined(THREADS) || defined(ATOMIC)
    std::array <std::thread, NTHREADS> threads;
    for (size_t i = 0; i < NTHREADS; ++i) {
        threads[i] = std::thread([this] (Matrix &matr, Matrix &matr1, Matrix &matr2, size_t thread_numb) {
            product(matr, matr1, matr2, thread_numb);
        }, std::ref(answer), std::ref(*this), std::ref(matr), i);
    }

    for (size_t i = 0; i < NTHREADS; ++i) {
        threads[i].join();
    }
    #else
    #ifdef OPEN_MP
    #pragma omp parallel
    {
    #pragma omp for
    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            for (size_t k = 0; k < size; ++k) {
                answer.add(i, j, data[i][k] * matr.get(k, j));
            }
        }
    }
    }
    #else
    #ifdef XMMINTRIN
    __m256d *p1, *p2, v1, v2;
    double prod[4];
    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            v1 = _mm256_sub_pd(v1, v1);
            p1 = reinterpret_cast<__m256d*>(data[i]);
            p2 = reinterpret_cast<__m256d*>(matr2.data[j]);
            for (size_t k = 0; k < size; k += 4) {
                v2 = _mm256_mul_pd(*p1, *p2);
                v1 = _mm256_add_pd(v1, v2);
                p1++;
                p2++;
            }
            _mm256_store_pd(prod, v1);
            answer.set(i, j, prod[0] + prod[1] + prod[2] + prod[3]);
        }
    }
    #else
    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            for (size_t k = 0; k < size; ++k) {
                answer.add(i, j, data[i][k] * matr.get(k, j));
            }
        }
    }
    #endif
    #endif
    #endif

    return answer;
}

#if defined(THREADS) || defined(ATOMIC)
void product(Matrix &matr, Matrix &matr1, Matrix &matr2, size_t thread_numb) {
    size_t _size = matr.getSize();

    #ifdef ATOMIC
    while (true) {
    #else
    for (size_t i = thread_numb; i < _size ; i += NTHREADS) {
    #endif
        #ifdef ATOMIC
        size_t i = (size_t)row;
        if (i >= _size) {
            break;
        }
        row++;
        #endif
        for (size_t j = 0; j < _size; ++j) {
            for (size_t k = 0; k < _size; ++k) {
                matr.add(i, j, matr1.get(i, k) * matr2.get(k, j));
            }
        }
    }
}
#endif

double Matrix::get(size_t i, size_t j) {
    return data[i][j];
}

void Matrix::set(size_t i, size_t j, double value) {
    if (i < size && j < size) {
        data[i][j] = value;
    } else {
        std::cerr << "Wrong data index" << std::endl;
        exit(1);
    }
}

void Matrix::add(size_t i, size_t j, double value) {
    if (i < size && j < size) {
        data[i][j] += value;
    } else {
        std::cerr << "Wrong data index" << std::endl;
        exit(1);
    }
}

// Returns Matrix size
size_t Matrix::getSize() {
    return size;
}

// Returns random double between 0 and 1
double Matrix::random(void) {
    return (double)(rand())/RAND_MAX;
}

// Fills matrix with random numbers between 0 and 1
void Matrix::randomFill() {
    srand((unsigned int)time(0));

    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            data[i][j] = random();
        }
        
    }
}

// Zero matrix
void Matrix::zero() {
    #ifdef OPEN_MPI
    for (size_t i = 0; i < size * size; i++) {
        _data[i] = 0;
    }
    #else
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            data[i][j] = 0;
        }
    }
    #endif
}

// Prints matrix
void Matrix::print() {
    #ifdef OPEN_MPI
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            std::cout << _data[i * size + j] << " ";
        }
        std::cout << std::endl;
    }
    #else
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            std::cout << get(i, j) << " ";
        }
        std::cout << std::endl;
    }
    #endif
}

Matrix::~Matrix() {
    #ifndef OPEN_MPI
    delete[] data[0];
    delete[] data;
    #endif
}