#include <iostream>
#include <chrono>
#include "matrix.h"

bool has_mul_work;
size_t current_row, done_rows;

#ifdef OPEN_MPI
int world_size;
int world_rank;
#endif

#ifdef OPEN_MPI
Matrix product(Matrix matr1, Matrix matr2) {

    size_t n = matr1.size;
    Matrix answer(n);
    answer.zero();
    MPI_Scatter(matr1._data, n * n / world_size, MPI_DOUBLE, matr1._data, n * n / world_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(matr2._data, n * n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    for (size_t i = 0; i < size_t(n / world_size); ++i) {
        for (size_t j = 0; j < n; ++j) {
            for (size_t k = 0; k < n; ++k) {
                answer._data[i * n + j] +=  matr1._data[i * n + k] * matr2._data[k * n + j];
            }
        }
    }
    MPI_Gather(answer._data, n * n / world_size, MPI_DOUBLE, answer._data, n * n / world_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    return answer;
}
#endif

int main(int argc, const char * argv[]) {

    bool mode = true;

    #ifdef OPEN_MPI
    int errCode;
    if ((errCode = MPI_Init(NULL, NULL)) != 0) {
        return errCode;
    }

    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    if (world_size == 1) {
        std::cerr << "Error!" << std::endl;
        exit(1);
    }
    #endif

    int n;
    #ifdef OPEN_MPI
    if (!world_rank){
    #endif
        std::cin >> n;
    #ifdef OPEN_MPI
    }
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    #endif

    if (n == 0) {
        n = 16;
        mode = false;
    }

    #ifdef OPEN_MPI
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    #endif

    Matrix answer(n);
    answer.zero();

    auto start = std::chrono::steady_clock::now();
    if (mode) {

        Matrix matr1(n);
        Matrix matr2(n);

        #ifdef OPEN_MPI
        if (!world_rank) {
        #endif

            matr1.randomFill();
            matr2.randomFill();
            start = std::chrono::steady_clock::now();

        #ifdef OPEN_MPI
        }
        #endif

        #ifdef OPEN_MPI
        product(matr1, matr2);
        #else
        matr1 * matr2;
        #endif

        #ifdef OPEN_MPI
        if (!world_rank) {
        #endif

            auto end = std::chrono::steady_clock::now();
            auto diff = end - start;
            std::chrono::milliseconds time = std::chrono::duration_cast<std::chrono::milliseconds>(diff);
            std::cout << time.count() << "ms" << std::endl;

        #ifdef OPEN_MPI
        }
        #endif

    } else {

        Matrix matr1("test/matrix.in");
        Matrix matr2("test/matrix.in");
        Matrix matr3("test/matrix.out");
        #ifdef OPEN_MPI
        if (!world_rank) {
            matr1.world_size = world_size;
            matr2.world_size = world_size;
        #endif

            start = std::chrono::steady_clock::now();

        #ifdef OPEN_MPI
        }
        answer = product(matr1, matr2);
        #else
        bool ans = (matr1 * matr2 == matr3);
        #endif


        #ifdef OPEN_MPI
        if (!world_rank) {
                bool ans = (answer == matr3);
        #endif
                auto end = std::chrono::steady_clock::now();
                auto diff = end - start;
                std::chrono::milliseconds time = std::chrono::duration_cast<std::chrono::milliseconds>(diff);
                std::cout << (ans ? "YES" : "NO") << std::endl;
                std::cout << time.count() << "ms" << std::endl;
        #ifdef OPEN_MPI
        }
        #endif
    }


    #if defined(OPEN_MPI)
    MPI_Finalize();
    #endif
    return 0;
}