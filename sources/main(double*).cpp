#include <iostream>
#include <chrono>
#include "matrix.h"

int main(int argc, const char * argv[]) {
    int n;
    std::cin >> n;

    if (n) {

        Matrix matr1(n);
        matr1.randomFill();
        
        Matrix matr2(n);
        matr2.randomFill();
        
        //Matrix answer(n);
        auto start = std::chrono::steady_clock::now();
        //answer =
        matr1 * matr2;
    	auto end = std::chrono::steady_clock::now();
    	auto diff = end - start;
    	std::chrono::milliseconds time = std::chrono::duration_cast<std::chrono::milliseconds>(diff);
    	std::cout << time.count() << "ms" << std::endl;

        //answer.print();
        // multiplies them and measures time spent
    } else {
        Matrix in("test/matrix.in");
        Matrix out("test/matrix.out");
        auto start = std::chrono::steady_clock::now();
        bool ans = ((in * in) == out);
    	auto end = std::chrono::steady_clock::now();
    	auto diff = end - start;
    	std::chrono::milliseconds time = std::chrono::duration_cast<std::chrono::milliseconds>(diff);
    	std::cout << (ans ? "YES" : "NO") << std::endl;
        std::cout << time.count() << "ms" << std::endl;
    }
}